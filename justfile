_default: help

#################################
# Building and publishing the OS
#################################

[private]
@codegen:
    tools/codegen

# Generate signing keys
@keygen: codegen
    tools/keygen

# Build the OS (alias: b)
@build +WHAT: codegen
    tools/build {{WHAT}}
    
[private]
alias b := build

# Generate DDIs, and publish to pub/ (alias: p)
@publish +WHAT: codegen
    tools/publish {{WHAT}}
    
[private]
alias p := publish
    
[private]
alias m := make

# Upload the published OS (alias: u)
@upload +WHAT="all":
    echo "NOT YET IMPLEMENTED!"
    exit 1

[private]
alias u := upload

# Build then publish the OS (alias: m)
@make +WHAT: (build WHAT) (publish WHAT)

#################################
# Updating packages
#################################

@_untrack:
    [ -f ./project.refs ] && rm ./project.refs || true
    [ -f ./junction.refs ] && rm ./junction.refs || true

# Manually update packages, then track everything (alias: up)
@update: _untrack && track
    tools/manual-updates

[private]
alias up := update

# Track all packages
@track: _untrack codegen
    tools/update-iana-data
    bst source track junctions/bst-plugins.bst
    bst source track junctions/*.bst
    bst source track
    tools/sort_project_refs.py

#################################
# Development helpers
#################################

# Check out a built bst element
@checkout ELEMENT: codegen
    [ -d result ] && rm -rf result/ || true
    bst --no-strict artifact checkout -d none --no-integrate --directory result {{ELEMENT}}

# Change kernel config options for a given board
@kconf BOARD: codegen
    tools/kconf {{BOARD}}

#################################
# Virtual machine
#################################

removable := "false"
tpm2 := "true"

# Create a virtual machine for specified unit
@create-vm UNIT VERSION="": && run-vm
    tools/vm/make {{UNIT}} "{{VERSION}}" "{{removable}}" "{{tpm2}}"

# Run an existing virtual machine, previously created by create-vm
@run-vm:
    tools/vm/run

#################################
# Documentation
#################################

# Show this help document
@help:
    echo "carbonOS Build System"
    echo "USAGE: just RECIPE [ARGS...]"    
    echo
    just -lu
    echo    
    echo -e "Some recipies take a list of units to operate on: \e[35m+\e[36mWHAT\e[0m"
    echo "You can use the following patterns in this list:"
    echo -e "    all           \e[34m# All units\e[0m"
    echo -e "    \e[36mBOARD\e[0m         \e[34m# All variants of a given board\e[0m"
    echo -e "    @\e[36mARCH\e[0m         \e[34m# All units of a given architecture\e[0m"
    echo -e "    \e[36mBOARD\e[0m/\e[36mVARIANT\e[0m \e[34m# A single specific unit\e[0m"
