# carbonOS 2023.1 (Alpha 5)

# carbonOS 2022.3 (Alpha 4)

- Linux 6.0 kernel
- Latest updates, bug fixes, security fixes
- Drop Graphite desktop environment, switch to GNOME
- Use ibus to enable complex input methods such as pinyin, anthy, unikey, etc.
- Make systemd-oomd friendlier
- Package distrobox, for easy pet-container management
- Remove nsbox for now (waiting for a rewrite)

# carbonOS 2022.2 (Alpha 3)

- Linux 5.19 kernel
- Redone kernel configuration: enabled missing drivers, disabled extra drivers, overall more sensible
- Latest security fixes
- Include intel microcode updates
- Downloadable debuginfo
- Thermal management for Intel CPUs
- Fingerprint scanner support (PARTIAL! No support in gde-greeter yet)
- Support for dual-gpu setups (PARTIAL! Missing integration w/ GDE)
- Redo font selection & reconfigure fontconfig; we now support CJK scripts!
- Split out system bootstrap, and generate better debuginfo
- Initial NVIDIA GPU support

# carbonOS 2022.1 (Alpha 2)

- Linux 5.16 kernel
- GNOME 42 platform
- Latest security fixes
- Graphite Desktop Environment Updates:
    - Official dark-mode support!
    - Implemented OSD popup to visually indicate volume and brightness adjustments
    - Upgrade many of the built-in apps to use GTK4 and libadwaita
    - Restyle GDE shell elements to fit in with new libadwaita theme
    - New included apps: Disk Usage Analyzer
- System update manager updates:
    - `updatectl rollback` command to easily uninstall system updates
    - Integration with GNOME Software
- Added podman, to provide a familiar docker-like environment for developers
- Added support for thunderbolt devices
- Added rtkit, which allows parts of the user's session to give themselves high-performance realtime scheduling
- Drop doas; all privilege escalation in carbonOS is now managed through polkit and pkexec
- Curated the list of built-in codecs (See issue #20)
- Added support for Vulkan graphics

# carbonOS 2021.1 (Alpha)

- Complete source-code restructuring
- Enable many different build-time security options and speed optimizations
- Remove debugging information from the binaries, making the OS much smaller
- Implement nsbox command environment
- Settings app
- Switch carbonOS to btrfs
- Enable filesystem compression (reduce size of logs, cache, and the OS; increase drive longevity)
- Gracefully handle system OOM: swap-on-zram and systemd-oomd
- Replace sudo with doas
- Replace pulseaudio with pipewire
- Update boot animation to show carbonOS logo if no firmware logo is available
- Implement system updates service and CLI tool
- New Graphite login screen
- New graphical installer and initial setup app
- Bug fixes all throughout the system
- Many, many more changes! This was a huge release!

# carbonOS 2020.1 (Pre-alpha)

- Initial release!
