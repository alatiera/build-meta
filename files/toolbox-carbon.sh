[ "$BASH_VERSION" != "" ] || return 0
[ "$PS1" != "" ] || return 0
[ "$(id -u)" -ge 1000 ] || return 0

toolbox_config="$HOME/.config/toolbox"
host_welcome_stub="$toolbox_config/host-welcome-shown"
toolbox_welcome_stub="$toolbox_config/toolbox-welcome-shown"

if [ -f /run/ostree-booted ] && [ ! -f "$host_welcome_stub" ]; then
    echo ""
    echo "Welcome to carbonOS! This terminal is running on the"
    echo "host system. You may want to try out the Toolbox for a directly"
    echo "mutable environment that allows package management with Fedora's DNF."
    echo ""
    echo "To enter the Toolbox, run 'toolbox enter'."
    echo ""
    printf "For more information, see the "
    # shellcheck disable=SC1003
    printf '\033]8;;https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/\033\\documentation\033]8;;\033\\'
    printf ".\n"
    echo ""

    mkdir -p "$toolbox_config"
    touch "$host_welcome_stub"
fi

if [ -f /run/.containerenv ] && [ -f /run/.toolboxenv ]; then
    # Set up prompt
    [ "$PS1" = "\\s-\\v\\\$ " ] && {
      PS1='[\u@\h:\W]\$ '
      for i in `seq 2 $SHLVL`; do PS1="|$PS1"; done
      unset i
    }
    PS1=$(printf "\U1F9F0%s" "$PS1")

    if ! [ -f "$toolbox_welcome_stub" ]; then
        clear -x # Clear the terminal to make it look nice but keep the scrollback
        echo ""
        echo "Welcome to the Toolbox; a container where you can install and run"
        echo "all your tools."
        echo ""
        echo " - Use DNF in the usual manner to install command line tools."
        echo " - To create a new tools container, run 'toolbox create'."
        echo ""
        printf "For more information, see the "
        # shellcheck disable=SC1003
        printf '\033]8;;https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/\033\\documentation\033]8;;\033\\'
        printf ".\n"
        echo ""

        mkdir -p "$toolbox_config"
        touch "$toolbox_welcome_stub"
    fi
fi

unset toolbox_config
unset host_welcome_stub
unset toolbox_welcome_stub
