#!/usr/bin/env python3
# This script scans for applications installed in system directories
# to automatically generate mimeapps.list
# Usage: tools/gen-mimeapps.py > files/mimeapps.list

import os
import gi
gi.require_version("GLib", "2.0")
from gi.repository import Gio

###########################################################
# Quirks/Settings for this script

# Only look for apps in these directories:
quirk_datadirs = [
    "/usr/share"
]

# Skip these apps when considering mimetypes
quirk_skip_apps = [
#EX:    "org.myapp.App.desktop"
]

# Skip these mimetypes (leave them w/o a default set)
quirk_skip_types = [
    # carbonOS doesn't support these package formats:
    "application/x-app-package",
    "application/x-rpm",
    "application/x-redhat-package-manager",
    "application/x-deb",
    "application/vnd.snap"
]

# The script will use this array as reference on how to order apps relative
# to each other
quirk_rel_sort_order = [
    # Prioritize mounting disk images to writing out disk images
    "gnome-disk-image-mounter.desktop", "gnome-disk-image-writer.desktop"
]

# Manually override some mime types
quirk_manual_override = {
#EX:    "type/foobar": ["org.myapp.App.desktop"]
}

###########################################################

os.environ["XDG_DATA_DIRS"] = ":".join(quirk_datadirs)

types = {}
for app in Gio.AppInfo.get_all():
    if app.get_id() in quirk_skip_apps:
        continue
    for type in app.get_supported_types():
        if type in types:
            types[type] += [app.get_id()]
        else:
            types[type] = [app.get_id()]

for override in quirk_manual_override:
    types[override] = quirk_manual_override[override];

def _sort_key(elem):
    if elem in quirk_rel_sort_order:
        return quirk_rel_sort_order.index(elem)
    else: # Apps not in rel_sort_order will be sorted after rel_sort_order
        return 1000

print("[Default Applications]")
for type in types:
    if type in quirk_skip_types:
        continue
    apps = ";".join(sorted(types[type], key = _sort_key))
    print(f"{type}={apps}")
