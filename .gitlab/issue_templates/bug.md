# Summary

<!-- Describe your bug here -->

# How to Reproduce

<!-- Give instructions to reproduce your bug here -->

1. Do this
2. Do that
3. Do the third thing
4. Bug happens

# Version Information

Output from `updatectl status`:

```
<put output here!>
```

<!-- 
If `updatectl status` doesn't work, please give the following info:
- carbonOS version (i.e. 2023.1, or a git commit)
- carbonOS base variant (desktop, mobile, devel, etc)
- carbonOS kernel variant (mainline, etc)
-->

# Other Relevant Information

<!-- 
Put logs, screenshots, etc here. Please use code blocks (```),
because otherwise code and logs are hard to read. Please try to
use collabsible sections if it is very long!
-->

/label ~bug