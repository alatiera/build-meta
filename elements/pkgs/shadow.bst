kind: autotools

sources:
- kind: git_repo
  url: github:shadow-maint/shadow
  track: refs/tags/*.*
  exclude:
  - "*-rc*"
- kind: local
  path: files/shadow
  directory: carbon-config

depends:
- pkgs/acl.bst
- pkgs/pam.bst
- pkgs/libcap.bst
- pkgs/btrfs-progs.bst
- pkgs/cracklib.bst
- pkgs/libbsd.bst

build-depends:
- buildsystems/autotools.bst

variables:
  autogen: autoreconf -ivf
  conf-local: >-
    --sbindir=%{prefix}/sbin
    --with-btrfs
    --with-libcrack
    --disable-static
    --disable-man

config:
  configure-commands:
    (<):
    - | # Fix broken build w/ git
      sed -i 's|man ||' Makefile.am
  install-commands:
    (<):
    - | # Fix broken build w/ git
      sed -i 's|%{prefix}/sbin|%{bindir}|' src/Makefile
      sed -i 's|usbindir = ${prefix}/sbin|usbindir = ${prefix}/bin|' src/Makefile
    (>):
    - | # Install config files
      rm %{install-root}%{sysconfdir}/login.defs
      mkdir %{install-root}%{sysconfdir}/default
      cp carbon-config/login.defs %{install-root}%{sysconfdir}/login.defs
      cp carbon-config/useradd %{install-root}%{sysconfdir}/default/useradd
    - | # Remove groups command
      rm -rv %{install-root}%{bindir}/groups
    - | # Remove some of the default PAM config
      for service in \
        login passwd su
      do
        rm -v %{install-root}%{sysconfdir}/pam.d/$service
      done
    - | # Remove pam_selinux and pam_console from remaining config
      sed -i /pam_selinux/d %{install-root}%{sysconfdir}/pam.d/*
      sed -i /pam_console/d %{install-root}%{sysconfdir}/pam.d/*
    - | # Install PAM config into /usr
      mv %{install-root}%{sysconfdir}/pam.d %{install-root}%{libdir}/pam.d
    - | # Create empty subuid and subgid files for toolbox
      touch %{install-root}%{sysconfdir}/sub{u,g}id

public:
  setuid:
    files:
    - "%{bindir}/passwd"
    - "%{bindir}/gpasswd"
    - "%{bindir}/su"
    - "%{bindir}/chsh"
    - "%{bindir}/chfn"
    - "%{bindir}/chage"
    - "%{bindir}/newgrp"
    - "%{bindir}/expiry"
