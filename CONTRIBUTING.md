# Contributing

We're glad you're interested in contributing to carbonOS!
This document describes some of the guidelines you can follow to ensure the smoothest contribution experience!

# Communication

carbonOS development is coordinated via the development Matrix chat: [#carbonOS-devel:matrix.org](https://matrix.to/#/%23carbonOS-devel%3Amatrix.org). Please join the room if you plan on sticking around. We'd be happy to have you!

Aside from the Matrix chat, development also takes place over the [GitLab instance](https://source.carbon.sh).

# Non-code Contributions

Contributing package definitions isn't the only way you can contribute to carbonOS! We're also in need of:

- Translations
- Testing & bug reports
- Web development
- Community outreach

If you're interested in contributing to any of these roles, please reach out in the [#carbonOS-devel:matrix.org](https://matrix.to/#/%23carbonOS-devel%3Amatrix.org) Matrix room. Once the project grows, we'll formalize some of these procedures

# Code of Conduct

All interactions in this GitLab group or any other forum related to carbonOS must follow the [project code of conduct (TODO!!!)](https://gitlab.com/carbonOS/issues/-/issues/8). You should also follow the [GitLab Community Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/).

# Code format

We try to follow a certain order of directives for carbonOS package definitions. Here's the guideline for that:

```yaml
kind: foo
description: |
  Description goes here. If it's short, put it inline with the directive

# Sources can come in any order, with the following limitations:
# - Put the "primary" source of the package first
# - Order the fields inside of the source as shown
# - Some source kinds need to be put in the right place for them to behave correctly (i.e. gen-cargo-lock, go-vendor, cargo)
# - Put patches last, separate them w/ a blank line, and include the comment as given below
sources:
- kind: git_tag
  url: alias:path/to/git/repo
  track: branch
  match:
  - regex
  - regex
  exclude:
  - regex
  - regex
  submodules:
    path/to/submodule:
      url: alias:path/to/git/submodule/repo
  directory: dest/dir
- kind: tar
  url: alias:path/to/tarball
  base-dir: base/dir/in/tarball
  directory: dest/dir
- kind: local
  path: path/to/local/file
  directory: dest/dir
# manual-updates comments go here, w/o blank line. See more info below

# TODO: Drop patch when {commit lands in tag, issue closed, etc}
# {link to place where we can check whether or not ^^^^ happened}
- kind: patch
  path: path/to/patch

depends:
- # Build + Runtime dependencies go here
- # For languages that only have runtime deps (like python), try to put them here too

runtime-depends:
- # Runtime-only dependencies go here

build-depends:
- # Build dependencies go here
- buildsystems/foobar.bst # buildsystem always comes last

environment:
  VARIABLE: value
  # Env vars go here...

variables:
  some-other-variable: value
  {conf,meson,cmake}-local: >- # If only one short flag, put inline
    --buildsystem-flags-go-here
    --the lines will be combined
  # The {conf,meson,cmake}-local variables should come last

config:
  configure-commands:
  - # configure commands go here
  - | # if multiline configure command, put comment here
    more configure commands go here
    this one is multi-line!
  build-commands:
  - # build commands go here
  install-commands:
  - # install comands go here
  - "%{install-extra}" # If overriding install-commands, put this here! Otherwise not needed

public:
  initial-script:
    script: # initial-script-goes-here
  bst:
    split-rules:
      ...
```

Try to keep the file as small as possible. Lots of reasonable defaults are included in the project. An average package (using the meson build system) will look something like this:

```yaml
kind: meson

sources:
- kind: git_tag
  url: some-git-instance:package-name
  track: main

depends:
- pkgs/somelib.bst
- pkgs/someotherlib.bst

build-depends:
- buildsystems/meson.bst

variables:
  meson-local: >-
    -Dsome-option=enabled
    -Ddocumentation=disabled
```

# General packaging policy

Try to follow these guidelines:

- Try to pick the most automatic source types (i.e. git_tag, git, gnu, pypi, cpan, etc), over those that need manual updates (i.e. tar)
- Prefer build systems like meson and cmake over build systems like autotools, and avoid custom build systems. If the upstream gives good reason (i.e. "we only support our own custom build system" or "the new cmake build system isn't as feature-complete as autotools") you should follow their advice
- Avoid patches if possible
- Set up tracking such that the element follows _stable_ releases

# Manual update directives

When you cannot select a source type that does automatic tracking, you must leave comments about how to track the source. There's a script (`tools/manual-updates`) that then helps the maintainers keep these packages up to date with minimal effort. Here's guidance about the special "magic comments" you can put:

- `# release-monitoring: <id>`. This is a link to a release-monitoring.org project: `https://release-monitoring.org/project/<id>/`. The manual-updates script will query release-monitoring.org for version information about the project, and present it to the maintainer before allowing them to manually upgrade the package
- `# manual-updates`. This is a comment that forces the manual-updates script to present this package to the maintainer. This is useful in combo w/ a "TODO" comment that requires the maintainer's attention the next time they upgrade the distro's packages
- `# skip-release-monitoring`. This is a comment that forces the manual-updates script to skip this package when it would have otherwise presented it to the maintainer. This is useful in packages that, for example, are no longer being updated, or have an oddly-named "main" branch

If none of the "magic comments" are applicable to what you're packaging, please leave a comment w/ a link to somewhere that the maintainer can look at for tracking information.

# Commit messages

Please try to follow [GNOME's commit guidelines](https://wiki.gnome.org/Git/CommitMessages). Here are some of the highlights:

Commit messages should:

- Be written in the imperative form (example: "Add feature" rather than "Adding feature" or "Added feature")
- Be clear enough that the purpose of the commit can be understood years later
- If related to a specific component, include a "tag" before the short summary of the commit: `tag: Short summary`. This is optional
- If more detail is necessary, include a detailed message for the commit. Include a blank line between the short summary and the detailed message.

Example of a good commit message:

```
foo: Work around build failure

After foo-dep was updated, foo stopped building correctly. Turns
out that foo-dep's API changed. This patch temporarily gets the build
working again, at least until foo is updated to use the new API.

Fixes https://gitlab.com/carbonOS/build-meta/-/issues/1234
```
